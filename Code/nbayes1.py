#!usr/bin/env python3
# -*- coding:utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pylab as pl
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score,\
    classification_report, confusion_matrix
from sklearn.model_selection import cross_val_score
# 完整的pipeline
'''preparing data'''
data = pd.read_csv('nbayes_data.csv',header=None)
print(data.iloc[:,2].value_counts())
X = data.iloc[:,:-1].to_numpy()
y = data.iloc[:,-1].to_numpy()
# split data
X_train, X_test, y_train, y_test =\
    train_test_split(X, y, test_size=0.20, random_state=53)

'''training the model'''
clf = GaussianNB()
clf.fit(X_train,y_train)

'''making predictions'''
y_pred = clf.predict(X_test)
# print(y_pred)

'''evaluating the model'''
print('Accuracy: %.2f' % accuracy_score(y_test, y_pred))
clf.score(X_test, y_test) # 2nd way to calculate accuracy
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

'''visualization'''
def visual(X,y):
    '''visualization'''
    # Plot the decision boundary. For that, we will asign a color
    # to each point in the mesh [x_min, x_max] x [y_min, y_max].
    x_min, x_max = X[:,0].min() - 0.1, X[:,0].max() + 0.1
    y_min, y_max = X[:,1].min() - 0.1, X[:,1].max() + 0.1
    h = 0.02 # step size in the mesh
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),\
                         np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Put the result into a color plot
    pl.figure(2)
    pl.set_cmap(pl.cm.Paired)
    pl.pcolormesh(xx, yy, Z)
    # Plot the testing points
    for i in range(len(X)):
        if y[i] == 0:
            _ = pl.scatter(X[i,0], X[i,1], c='red', marker='x')
        elif y[i] == 1:
            _ = pl.scatter(X[i,0], X[i,1], c='blue', marker='+')
        elif y[i] == 2:
            _ = pl.scatter(X[i,0], X[i,1], c='green', marker='o')
        else:
            _ = pl.scatter(X[i,0], X[i,1], c='c', marker='^')
    pl.xlim()
    pl.ylim()
    pl.title('naive bayes classifier model boundaries')

visual(X_test,y_test)
plt.show()

'''S-fold cross validation''' 
def cross_valid(classifier):
    num_folds = 3
    accuracy_values = cross_val_score(classifier,\
            X, y, scoring='accuracy', cv=num_folds)
    print("Accuracy: " + str(round(100*accuracy_values.mean(), 2)) + "%")

    precision_values = cross_val_score(classifier,\
            X, y, scoring='precision_weighted', cv=num_folds)
    print("Precision: " + str(round(100*precision_values.mean(), 2)) + "%")

    recall_values = cross_val_score(classifier,\
            X, y, scoring='recall_weighted', cv=num_folds)
    print("Recall: " + str(round(100*recall_values.mean(), 2)) + "%")

    f1_values = cross_val_score(classifier,\
            X, y, scoring='f1_weighted', cv=num_folds)
    print("F1: " + str(round(100*f1_values.mean(), 2)) + "%")

cross_valid(clf)
